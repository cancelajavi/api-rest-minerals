'use strict'

var mongoose = require("mongoose")
var Schema = mongoose.Schema

var MineralSchema = Schema({
    name: String,
    description: String,
    obtained: Boolean,
    location: String,
    image: String
})

module.exports = mongoose.model('Mineral', MineralSchema)