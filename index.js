'use strict'

var mongoose = require("mongoose");
var app = require("./app");

var port = process.env.PORT || 3700;

mongoose.connect('mongodb://localhost:27017/minerals', (err, res) => {
    if(err){
        throw err;
    } else {
        console.log("Succesfully connected!");
        app.listen(port, () => {
            console.log("Minerals api listening on port: "+port);
        });
    }
});
