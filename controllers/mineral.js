'use strict'

var path = require('path')
var Mineral = require("../models/mineral")

function getMineral(req, res){
    var mineralId = req.params.id

    Mineral.findById(mineralId, (err, mineral)=>{
        if(err){
            res.status(500).send({
                message: "Error in the request"
            })
        } else {
            if(!mineral){
                res.status(404).send({
                    message: "Mineral not in DB"
                })
            } else {
                res.status(200).send({mineral})
            }
        }
    })
}

function getMinerals(req, res){

    Mineral.find({}, (err, minerals)=>{
        if(err){
            res.status(500).send({
                message: "Error in the request"
            })
        } else {
            if(!minerals){
                res.status(404).send({
                    message: "There is no minerals in DB"
                })
            } else {
                res.status(200).send({minerals})
            }
        }
    })
}

function saveMineral(req, res){
    var mineral = new Mineral()
    var params = req.body

    mineral.name = params.name
    mineral.description = params.description
    mineral.obtained = params.obtained
    mineral.location = params.location
    mineral.image = null

    mineral.save((err, mineralStored)=>{
        if(err){
            res.status(500).send({
                message: "Error saving the mineral"
            })
        } else{
            if(!mineralStored){
                res.status(404).send({
                    message: "Mineral not saved"
                })
            } else {
                res.status(200).send({
                    mineral: mineralStored
                })
            }
        }
    })
}

function updateMineral(req, res){
    var mineralId = req.params.id

    var update = req.body

    Mineral.findByIdAndUpdate(mineralId, update, (err, mineralUpdated)=>{
        if(err){
            res.status(500).send({
                message: "Error updating the mineral"
            })
        } else {
            if(!mineralUpdated){
                res.status(404).send({
                    message: "Mineral not updated"
                })
            } else {
                res.status(200).send({
                    mineral: mineralUpdated
                })
            }
        }
    })
}

function deleteMineral(req, res){
    var mineralId = req.params.id

    var update = req.body

    Mineral.findByIdAndRemove(mineralId, (err, mineralDeleted)=>{
        if(err){
            res.status(500).send({
                message: "Error removing the mineral"
            })
        } else {
            if(!mineralDeleted){
                res.status(404).send({
                    message: "Mineral not removed"
                })
            } else {
                res.status(200).send({
                    mineral: mineralDeleted
                })
            }
        }
    })
}

function uploadImage(req, res){
    var file_name = 'Not uploaded...'
    var mineralId = req.params.id
    
    if(req.files){
        var file_path = req.files.image.path
        var file_split = file_path.split('\\')
        var file_name = file_split[1]

        Mineral.findByIdAndUpdate(mineralId, {image: file_name}, (err, mineralUpdated)=>{
        if(err){
            res.status(500).send({
                message: "Error updating the mineral"
            })
        } else {
            if(!mineralUpdated){
                res.status(404).send({
                    message: "Mineral not updated"
                })
            } else {
                res.status(200).send({
                    mineral: mineralUpdated
                })
            }
        }
        })
    } else {
        res.status(200).send({
            message: 'Errooooor'
        })
    }
}
var fs = require('fs')
function getImageFile(req, res){
    var imageFile = req.params.imageFile

    fs.exists('./uploads/'+imageFile, (exists)=>{
        if(exists){
            res.sendFile(path.resolve('./uploads/'+imageFile))
        } else {
            res.status(404).send({
                message: 'No image with the name: '+imageFile
            })
        }
    })
}

module.exports = {
    getMineral,
    getMinerals,
    saveMineral,
    updateMineral,
    deleteMineral,
    uploadImage,
    getImageFile
}