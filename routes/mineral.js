'use strict'

var express = require('express')
var MineralController = require('../controllers/mineral')
var api = express.Router()

var multipart = require('connect-multiparty')
var multipartMiddleware = multipart({
    uploadDir: './uploads'
})

api.get('/mineral/:id', MineralController.getMineral)

api.get('/minerals', MineralController.getMinerals)

api.post('/mineral', MineralController.saveMineral)

api.put('/mineral/:id', MineralController.updateMineral)

api.delete('/mineral/:id', MineralController.deleteMineral)

api.post('/upload-image/:id', multipartMiddleware , MineralController.uploadImage)

api.get('/get-image/:imageFile', MineralController.getImageFile)

module.exports = api;